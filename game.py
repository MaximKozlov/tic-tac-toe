from tkinter import *
from tkinter import messagebox

def Win(Put):
    """ Check of win """
    
    WinCords = ((0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
                (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6))
    for Each in WinCords:
        if (btns[Each[0]]['text'] == btns[Each[1]]['text']\
            == btns[Each[2]]['text'] == Put):

            messagebox.showinfo('GameOver', 'win ' + Put)

            return True
    return False

def get_symbol():
    '''символ текущего игрока и смена хода'''
    global xod_first_user
    
    ans = 'O'
    if xod_first_user:     
        ans = 'X'
        
    xod_first_user = not xod_first_user
    return ans


def handler(i):
    '''обработчик события нажатия на кнопку'''
    print(i)
    s = get_symbol()
    if not btns[i]['text']:
        btns[i].config(text = s)
        active_btns.remove(i)
        if Win(s):
            refresh()
        if not active_btns:
            messagebox.showinfo('GameOver', 'draw')
            refresh()
     

def create_panel():
    '''создание кнопок'''
    def decor_handler(func, *args):
        return lambda: func(*args)

    btns = [None]*9
    for x in range(9):
        btns[x] = Button(frame,
                         command=decor_handler(handler, x), width=20,
                         height=10, bg='lightgreen')
        btns[x].grid(row=x//3, column=x%3)

    return btns

def refresh():
    '''начать заново'''
    global xod_first_user, btns, active_btns
    btns = create_panel()
    xod_first_user = True
    active_btns = list(range(9))


root = Tk()
root.title(u'Крестики нолики')
root.geometry('+300+200')
root.minsize(480, 508)
root.maxsize(480, 508)

frame = Frame(root)
frame.pack(expand=1, padx = 2, pady = 13)

btns = []
active_btns = []
xod_first_user = True

refresh()

root.mainloop()
